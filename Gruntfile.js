module.exports = function(grunt) {
     // Load Grunt tasks declared in the package.json file
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
    grunt.initConfig({
      concat: {
        options: {
          separator: ';'
        },
        dist: {
          src: [ 'js/main.js'],

          dest: 'js/temp-concat-javascript.js'
        }
      },
      uglify: {
        my_target: {
          files: {
            'js/javascript.min.js': 'js/temp-concat-javascript.js'
          }
        }
      },
      watch: {
        compass: {
            files: ['scss/{,*/}*.{scss,sass}', 'js/{,*/}*.js'],
            tasks: ['compass:dist', "concat", "uglify"]
        },
        options: {
            spawn:false
        }
      },
      compass: {
          dist:{
            options: {
              sassDir: 'scss',
              cssDir: 'css',
              imagesDir: 'img',
              fontsDir: 'fonts',
              relativeAssets: true
            }
          }
      },
      browserSync: {
          bsFiles: {
              src : [ 'css/*.css',
                      'js/**/*.js',
                      'img/**/*.jpg',
                      'img/**/*.png',
                      '.php',
                      '.html'
                      ]
          },
          options: {
            watchTask: true,
            proxy: "localhost/ludimilla/ludireta"//,
          }
      }
   



    });


    // grunt.loadNpmTasks('grunt-contrib-concat');
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    // grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-contrib-compass');
    // grunt.registerTask('default', ['concat', 'uglify', 'watch', 'compass']);
    grunt.registerTask('default', ['browserSync','watch']);
};
