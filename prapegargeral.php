
<?php


?>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>A Danada Sou Eu :: Manual da Lud pra Pegar Geral</title>
        <meta name="description" content="Pra quem ficou solteiro, tá na seca ou só quer atualizar suas táticas: dicas danadas pra sair pegando geral!">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        
        <link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
        <link rel="manifest" href="manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">


        <meta property="og:locale" content="pt_BR">
 
        <meta property="og:url" content="http://adanadasoueu.com.br/prapegargeral">
         
        <meta property="og:title" content="A Danada Sou Eu :: Manual da Lud pra Pegar Geral">
        <meta property="og:site_name" content="Pra Pegar Geral">
         
        <meta property="og:description" content="Pra quem ficou solteiro, tá na seca ou só quer atualizar suas táticas: dicas danadas pra sair pegando geral! ">
        <meta property="og:image" content="http://adanadasoueu.com.br/img/album_cover.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:type" content="website">
        <meta property="fb:app_id" content="1694960667490354">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/style.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>




    </head>
    <body class="prapegargeral">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="wrap">
            <header class="logo-header">
                <h1 class="ir manual-logo">Manual da Lud pra Pegar Geral</h1>
            </header>
            <article>
                <p>Pra quem ficou solteiro, tá na seca ou só quer atualizar suas táticas: dicas danadas pra sair pegando geral!</p>
                <button class="aprender-comigo">Clique aqui pra aprender comigo</button>
                <div class="share">
                    <span>Só pega geral quem compartilha as danadices! Clique no Facebook ou Twitter para compartilhar o manual e liberar o download.</span>
                    <button class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</span></button><button class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i>
    <span>Twitter</span></button>
                    <a id="baixar-manual" class="disabled" href="Ludmilla_Manual_Da_Lud_Pra_Pegar_Geral.pdf" download >Baixar o Manual</a>
                </div>
            </article>
            <aside>
                <div class="where-album">
                    <img src="img/album_cover.png" alt="Ludimilla - A Danada Sou Eu"/>
                    <p>Vou largar uma indireta bem direta pra vocês: <strong>meu novo álbum já está disponível</strong> para pré-venda e vocês podem encomendar aqui:</p>
                    <div class="album-links">
                        <ul>
                            <li><a target="_blank" class="ir cultura" href="http://www.livrariacultura.com.br/p/a-danada-sou-eu-46347755">Livraria Cultura</a></li>
                            <li><a target="_blank" class="ir itunes" href="https://itunes.apple.com/br/album/a-danada-sou-eu/id1146694597">iTunes</a></li>
                            <li><a target="_blank" class="ir saraiva" href="http://www.saraiva.com.br/ludmilla-a-danada-sou-eu-9373301.html">Saraiva</a></li>
                            <li><a target="_blank" class="ir google" href="https://play.google.com/store/music/album/Ludmilla_A_danada_sou_eu?id=Beeiwu2ym6jxxlc7v6zmuiyx7oy&hl=pt_BR">Google Play</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="channel">
                        <ul>
                            <li><a target="_blank" class="facebook" href="https://www.facebook.com/OficialLudmilla/?fref=ts"><i class="fa fa-facebook" aria-hidden="true"></i><span class="ir">Facebook</a></span></li>
                            <li><a target="_blank" class="instagram" href="https://www.instagram.com/ludmilla/"><i class="fa fa-instagram" aria-hidden="true"></i><span class="ir">Instagram</a></span></li>
                            <li><a target="_blank" class="snapchat" href="https://www.snapchat.com/add/OficialLudmilla/"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i><span class="ir">Snapchat</span></a></li>
                            <li><a target="_blank" class="twitter" href="https://twitter.com/ludmilla"><i class="fa fa-twitter" aria-hidden="true"></i><span class="ir">Twitter</a></span></li>
                        </ul>
                </div>
            </aside>
        </div>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>

        <!-- Add fancyBox -->
        <link rel="stylesheet" href="js/vendor/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
        <script type="text/javascript" src="js/vendor/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

        <!-- Optionally add helpers - button, thumbnail and/or media -->
        <link rel="stylesheet" href="js/vendor/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
        <script type="text/javascript" src="js/vendor/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
        <script type="text/javascript" src="js/vendor/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

        <link rel="stylesheet" href="js/vendor/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
        <script type="text/javascript" src="js/vendor/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>


        <script src="js/plugins.js"></script>
        <script src="js/javascript.min.js"></script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-86131132-1', 'auto');
          ga('send', 'pageview');

        </script>        

        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '1694960667490354',
              xfbml      : true,
              version    : 'v2.8'
            });
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>
    </body>
</html>
