
var jsonData;
function onResizeWindow()
{
	if ($(window).width() < 966) onResizeMobile();
	else onResizeDesk();
}

function onResizeMobile()
{
	$('.hotsite-logo').height($('.hotsite-logo').width()*279/549);
}

function onResizeDesk()
{
	$('aside').css("left", "auto");
	$('.hotsite-logo').height(199);// baseado no crop da tela desk
	if($(window).outerWidth() >= 1425) // é para ser o 1240 do css -- mudei para 1440
	{
		$('.hotsite-logo').height(290);// baseado no crop da tela desk
	}
	if ($(window).outerWidth() >= 1000 && $(window).outerWidth() < 1425) 
	{
		$('aside').css("left", $(window).outerWidth()/2 + 300);
	}
	else if ($(window).outerWidth() >= 1425) 
	{
		$('aside').css("left", $(window).outerWidth()/2 + 500);
	}
}

function loadJson()
{
	$.getJSON('js/frases.json', function(data) {
	    jsonData = data;
	    var alvo = [];
	    for (var i = 0; i < jsonData.length; i++) {
	    	console.log("tenta ", jsonData[i].ALVO);
	    	if ($.inArray(jsonData[i].ALVO, alvo) == -1) 
	    		{
	    			alvo.push(jsonData[i].ALVO);
	    		}
	    };
	    console.log(alvo);
	    alvo.sort();
	    for (var i = 0; i < alvo.length; i++) {
	    	$('.alvo').append("<option>"+alvo[i]+"</option>");
	    };
	});
}

function openFaceSharer(msg, folder)
{
	if (!folder) folder = "";
	console.log("folder", folder == "");
  FB.ui({
    method: 'share',
    display: 'popup', 
	quote: msg,
	    href: 'http://adanadasoueu.com.br'+folder,
	  }, function(response){});
}

function load4Whats(alvo)
{
	$('.indireta-para option').each(function(index){
		if(index >0)$(this).remove();
	});
	var tema = [];
	for (var i = 0; i < jsonData.length; i++) {
		if (jsonData[i].ALVO == alvo)
		{
			tema.push(jsonData[i].TEMA);
		}
	};
	tema = $.unique(tema);
	tema.sort();
    for (var i = 0; i < tema.length; i++) {
    	$('.indireta-para').append("<option>"+tema[i]+"</option>");
    };
	$('.indireta-para').removeAttr("disabled");
}

var indirects;
function loadIndirect(tema)
{
	indirects = [];
	for (var i = 0; i < jsonData.length; i++) {
		if (jsonData[i].TEMA == tema)
		{
			indirects.push(jsonData[i].FRASE);
		}
	};
	console.log(indirects);
	$('.largar-indireta').removeAttr("disabled");
}

function randomIndirect()
{
	var frase = indirects[Math.floor(Math.random()*indirects.length)];
	$('article textarea').val(frase);
}

jQuery(document).ready(function($) {
	
	// $("body").height($("html").outerHeight())
	$( window ).resize(onResizeWindow);
	onResizeWindow();
	if ($(window).width() >= 966 && $(".spotify iframe").length == 0) 
	{
		// $(".spotify").prepend('<iframe src="https://embed.spotify.com/?uri=spotify%3Auser%3Aspotify%3Aplaylist%3A3ybCH3dcvkZ665IIu0Onqq" width="184" height="280" frameborder="0" allowtransparency="true"></iframe>
		$(".spotify").prepend('<iframe src="https://embed.spotify.com/?uri=spotify%3Auser%3Aludmillaoficial%3Aplaylist%3A3ybCH3dcvkZ665IIu0Onqq" width="184" height="280" frameborder="0" allowtransparency="true"></iframe>
');
	}
	$(".ludiretas .share .facebook").click(function(e){ 
		if ($('article textarea').val() != '')
		{
			e.preventDefault(); 
			openFaceSharer($('article textarea').val());
		}
	} );
	$(".ludiretas .share .twitter").click(function(e){ 
		if ($('article textarea').val() != '')
		{
			e.preventDefault(); 
			window.open("https://twitter.com/home?status="+encodeURI($('article textarea').val()).replace("#", "%23"));
		}
	} );
	loadJson();
	$(".alvo").change(function(){
		load4Whats($(".alvo").val());
	});
	$(".indireta-para").change(function(){
		loadIndirect($(".indireta-para").val());
	});
	$(".largar-indireta").click(function(){
		randomIndirect();
	});

	$("a.inlinefancybox").fancybox({
		'hideOnContentClick': true,
		'maxWidth': '60%',
		'minWidth': '200px'
	});

	if ($('.prapegargeral').length > 0)
	{
		$('.prapegargeral .aprender-comigo').click(function(e){
			e.preventDefault();
			$('.prapegargeral .share').show(0);
		});

		$('#baixar-manual').click(function(e){
			if ($('#baixar-manual').hasClass("disabled"))
			{
				e.preventDefault();
			}
			else
			{
				ga('send', 'event', 'Download', 'PDF', this.href);
			}
		});
		$(".prapegargeral .share .facebook").click(function(e){ 
			e.preventDefault(); 
			setTimeout(function(){$('#baixar-manual').removeClass("disabled");}, 2000);
			openFaceSharer("", "/prapegargeral");
		} );
		$(".prapegargeral .share .twitter").click(function(e){ 
			e.preventDefault(); 
			setTimeout(function(){$('#baixar-manual').removeClass("disabled");}, 2000);
			window.open("https://twitter.com/home?status="+encodeURI("Manual da Lud pra pegar geral http://adanadasoueu.com/prapegargeral").replace("#", "%23"));
		} );
	}
});